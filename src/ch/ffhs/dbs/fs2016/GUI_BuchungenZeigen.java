package ch.ffhs.dbs.fs2016;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.PreparedStatement;
import java.util.Calendar;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ch.danielhoop.sql.DataFrame;
import ch.danielhoop.sql.SQLUtils;


@SuppressWarnings("serial")
public class GUI_BuchungenZeigen extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private SQLUtils db;
	private JTextArea dayStaTxt, dayEndTxt, monStaTxt, monEndTxt, yeaStaTxt, yeaEndTxt;
	private PreparedStatement prst;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LookAndFeel.set();
					GUI_BuchungenZeigen frame = new GUI_BuchungenZeigen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_BuchungenZeigen() {
		
		new Thread( () -> {
			db = createNewSQLUtils();
			try{
				prst = db.getConnection().prepareStatement("select * from buchung where datum_anreise < ? and datum_abreise > ?;");
			} catch(Exception e){ showExceptionVisually(e); }
		}).start();

		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		
		int x, y;
		int INSET = 3;
		Dimension dimensionTxtAreas = new Dimension(35,15);
		
		
		// Set gridbaglayout
		contentPane.setLayout(new java.awt.GridBagLayout());
		// Prepare constraints variable for later use.
		//java.awt.GridBagConstraints c;
		// Prepare x and y
		x = 0;
		y = 0; // -1 would start at at top. 0 leaves one empy.
		
		// Anreise JLabel
		JLabel anreiseLabel = new JLabel("Datum Anreise:");
		anreiseLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT); // Schrift festlegen (rechts-, mittig, linksbündig)
		//expressionLabel.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM); // Schrift festlegen (oben, mittig, unten)
		java.awt.GridBagConstraints c = new java.awt.GridBagConstraints(); // Reset constraints
		c.insets = new java.awt.Insets(0, INSET, 0, INSET); // Ränder einfügen
		c.gridx = x; // Platz im GridBayLayout x-Achse
		c.gridy = ++y; // Platz im GridBayLayout y-Achse
		c.fill = java.awt.GridBagConstraints.BOTH; // Die Felder sollen vertikal UND horizontal vergrössert werden, wenn das Fenster vergrössert wird!
		c.weightx = 0;   // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der x-Achse haben?
		c.weighty = 0; // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der y-Achse haben?
		//c.gridwidth = java.awt.GridBagConstraints.RELATIVE; // Am Ende der x-Achse noch eine Zeile frei lassen.
		c.anchor = java.awt.GridBagConstraints.EAST; // Rechts verankern.
		c.insets = new java.awt.Insets(INSET, INSET, INSET, INSET); // Ränder einfügen
		contentPane.add(anreiseLabel, c);
		// Abreise JLabel
		JLabel abreiseLabel = new JLabel("Datum Abreise:");
		c.gridy = ++y; // Platz im GridBayLayout y-Achse
		contentPane.add(abreiseLabel, c);
		
		
		// Tag, Monat, Jahr
		x = 0;
		y = 0;
		JLabel tagLabel = new JLabel("Tag");
		c.gridx = ++x;
		c.gridy = y;
		contentPane.add(tagLabel, c);
		
		JLabel monatLabel = new JLabel("Monat");
		c.gridx = ++x;
		contentPane.add(monatLabel, c);
		
		JLabel jahrLabel = new JLabel("Jahr");
		c.gridx = ++x;
		contentPane.add(jahrLabel, c);
		
		JLabel emptyLabel = new JLabel("");
		c.gridx = ++x;
		c.weightx = 1;   // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der x-Achse haben?
		contentPane.add(emptyLabel, c);
		c.weightx = 0;
		
		
		// Date JText fields.
		dayStaTxt = new JTextArea("01");
		dayEndTxt = new JTextArea("31");
		monStaTxt = new JTextArea("01");
		monEndTxt = new JTextArea("12");
		yeaStaTxt = new JTextArea( ""+Calendar.getInstance().get(Calendar.YEAR) );
		yeaEndTxt = new JTextArea( ""+Calendar.getInstance().get(Calendar.YEAR) );
		
		dayStaTxt.setPreferredSize( dimensionTxtAreas );
		dayEndTxt.setPreferredSize( dimensionTxtAreas );
		monStaTxt.setPreferredSize( dimensionTxtAreas );
		monEndTxt.setPreferredSize( dimensionTxtAreas );
		yeaStaTxt.setPreferredSize( dimensionTxtAreas );
		yeaEndTxt.setPreferredSize( dimensionTxtAreas );
		
		x = 0;
		y = 1;
		c.gridy = y;
		c.gridx = ++x; contentPane.add(dayStaTxt, c);
		c.gridx = ++x; contentPane.add(monStaTxt, c);
		c.gridx = ++x; contentPane.add(yeaStaTxt, c);
		
		x = 0;
		y = 2;
		c.gridy = y;
		c.gridx = ++x; contentPane.add(dayEndTxt, c);
		c.gridx = ++x; contentPane.add(monEndTxt, c);
		c.gridx = ++x; contentPane.add(yeaEndTxt, c);
		
		JButton button = new JButton("Suche!");
		c.gridheight = 2;
		c.gridx = ++x;
		c.gridy = 1;
		contentPane.add(button, c);
		
		
		table = new JTable();
		c.gridy = 3;
		c.gridx = 1;
		c.gridwidth = 4;
		c.gridheight = 1;
		c.weightx = 1;   // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der x-Achse haben?
		c.weighty = 1;   // Welchen Anteil (0-1) soll diese Komponente beim vergrössern auf der x-Achse haben?
		
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setPreferredSize( new Dimension(400,150) );
		contentPane.add(scrollPane, c);
		
		
		// Add action to button
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				showBookingsinTable();
			}
		});
		// Set button as default button
		getRootPane().setDefaultButton(button);
		// Do action, when enter key is pressed!
		KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
		    @Override
		    public boolean dispatchKeyEvent(KeyEvent e) {
		      if(e.getID()==KeyEvent.KEY_RELEASED && e.getKeyCode()==KeyEvent.VK_ENTER ){
		    	  showBookingsinTable();
		      }
		      return false;
		    }
		});
		// Alternative, but does not work!
		contentPane.addKeyListener(new KeyAdapter(){
			@Override
			public void keyPressed(KeyEvent evt) {
				if(evt.getKeyCode() == KeyEvent.VK_ENTER) {
					//System.out.println("OK!!");
				   }				
			}
		});
		// Alternative, but does not work!
		contentPane.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
		        System.out.println("OK!!");		
			}
		});

		
		
		setContentPane(contentPane);
	}
	
	
	public void showBookingsinTable(){
		
		new Thread( () -> {
		try {
			String staDate = yeaStaTxt.getText() +"-"+ monStaTxt.getText() +"-"+ dayStaTxt.getText();
			String endDate = yeaEndTxt.getText() +"-"+ monEndTxt.getText() +"-"+ dayEndTxt.getText();			
			
			prst.setString(1, endDate);
			prst.setString(2, staDate);
			DataFrame df = SQLUtils.resultSetToDataFrame( prst.executeQuery() );
			
			df.printTable();
	    		    	
	    	SwingUtilities.invokeLater( () ->      table.setModel(new DefaultTableModel( df.getTable(), df.getColnames() ))       );
			
		} catch (Exception e) { showExceptionVisually(e); }
		}).start();
		
	}	
	
	@SuppressWarnings("unused")
	private void showExceptionVisually(Exception e, String message){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.INFORMATION_MESSAGE) );
		e.printStackTrace();
	}
	private void showExceptionVisually(Exception e){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE) );		
	}
	private SQLUtils createNewSQLUtils(){
		try { return new SQLUtils("com.mysql.jdbc.Driver",
					"jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017");
		} catch (Exception e) { showExceptionVisually(e); }
		return null;
	}
}