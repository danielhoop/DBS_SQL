package ch.ffhs.dbs.fs2016;

import javax.swing.JFrame;

public class LookAndFeel {

	public static void set() {
		try{
			// First try system Look and feel
			javax.swing.UIManager.setLookAndFeel( javax.swing.UIManager.getSystemLookAndFeelClassName() );
		} catch (Exception ex){
			// If system Look and Feel ist not available, try Nimbus, else keep the normal one.
			// For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
			try {
				for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
					System.out.println(info);
					if ("Nimbus".equals(info.getName())) { // Metal Nimbus
						javax.swing.UIManager.setLookAndFeel(info.getClassName());
						break;
					}
				}
			} catch (Exception ex1) {
				java.util.logging.Logger.getLogger(JFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
			}
		}
	}
}
