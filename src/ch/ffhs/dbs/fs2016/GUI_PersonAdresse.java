package ch.ffhs.dbs.fs2016;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import ca.odell.glazedlists.GlazedLists;
import ca.odell.glazedlists.swing.AutoCompleteSupport;
import ch.danielhoop.sql.DataFrame;
import ch.danielhoop.sql.SQLUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.HashMap;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class GUI_PersonAdresse extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldVorname;
	private JTextField textFieldNachname;
	private JTextField textFieldStrasse;
	private JTextField textFieldStrasseZusatz;
	private JTextField textFieldPostleitzahl;
	private JTextField textFieldOrt;
	private JTextField textFieldLand;
	private final String nameSeparator = "   ";
	private JTextField textFieldEmail;
	private JTextField textFieldTelefon;
	private JTextField textFieldKreditkartennummer;
	private JTextField textFieldKreditkarteSicherheit;
	private String persId;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				LookAndFeel.set();
				new GUI_PersonAdresse();
			}
		});
	}

	/**
	 * Create the frame.
	 * @param frame 
	 */
	public GUI_PersonAdresse() {
		JFrame framePerson = new JFrame("Person Erfassen");
		framePerson.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		framePerson.setBounds(100, 100, 525, 510);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		framePerson.setContentPane(contentPane);
		contentPane.setLayout(null);
		framePerson.setVisible(true);
		
		JLabel labelVornameNachname = new JLabel("Vorname, Nachname");
		labelVornameNachname.setBounds(10, 11, 133, 14);
		contentPane.add(labelVornameNachname);
		
		JComboBox comboBoxVornameNachname = new JComboBox();
		comboBoxVornameNachname.setBounds(153, 10, 325, 17);
		contentPane.add(comboBoxVornameNachname);
		
		JLabel lblAnrede = new JLabel("Anrede");
		lblAnrede.setBounds(10, 36, 133, 14);
		contentPane.add(lblAnrede);
		
		JLabel lblVorname = new JLabel("Vorname");
		lblVorname.setBounds(10, 61, 133, 14);
		contentPane.add(lblVorname);
		
		JLabel lblNachname = new JLabel("Nachname");
		lblNachname.setBounds(10, 86, 133, 14);
		contentPane.add(lblNachname);
		
		JLabel lblStrasse = new JLabel("Strasse");
		lblStrasse.setBounds(10, 136, 133, 14);
		contentPane.add(lblStrasse);
		
		JLabel lblStrasseZusatz = new JLabel("Strasse Zusatz");
		lblStrasseZusatz.setBounds(10, 161, 133, 14);
		contentPane.add(lblStrasseZusatz);
		
		JLabel lblPostleitzahl = new JLabel("Postleitzahl");
		lblPostleitzahl.setBounds(10, 186, 133, 14);
		contentPane.add(lblPostleitzahl);
		
		JLabel lblOrt = new JLabel("Ort");
		lblOrt.setBounds(10, 211, 133, 14);
		contentPane.add(lblOrt);
		
		JLabel lblLand = new JLabel("Land");
		lblLand.setBounds(10, 236, 133, 14);
		contentPane.add(lblLand);
		
		JLabel lblGeschftlich = new JLabel("Gesch\u00E4ftlich");
		lblGeschftlich.setBounds(10, 261, 133, 14);
		contentPane.add(lblGeschftlich);
		
		JComboBox comboBoxAnrede = new JComboBox();
		comboBoxAnrede.setBounds(153, 33, 325, 17);
		contentPane.add(comboBoxAnrede);
		
		textFieldVorname = new JTextField();
		textFieldVorname.setBounds(153, 58, 325, 17);
		contentPane.add(textFieldVorname);
		textFieldVorname.setColumns(10);
		
		textFieldNachname = new JTextField();
		textFieldNachname.setColumns(10);
		textFieldNachname.setBounds(153, 83, 325, 17);
		contentPane.add(textFieldNachname);
		
		textFieldStrasse = new JTextField();
		textFieldStrasse.setColumns(10);
		textFieldStrasse.setBounds(153, 133, 325, 17);
		contentPane.add(textFieldStrasse);
		
		textFieldStrasseZusatz = new JTextField();
		textFieldStrasseZusatz.setColumns(10);
		textFieldStrasseZusatz.setBounds(153, 158, 325, 17);
		contentPane.add(textFieldStrasseZusatz);
		
		textFieldPostleitzahl = new JTextField();
		textFieldPostleitzahl.setColumns(10);
		textFieldPostleitzahl.setBounds(153, 183, 325, 17);
		contentPane.add(textFieldPostleitzahl);
		
		textFieldOrt = new JTextField();
		textFieldOrt.setColumns(10);
		textFieldOrt.setBounds(153, 208, 325, 17);
		contentPane.add(textFieldOrt);
		
		textFieldLand = new JTextField();
		textFieldLand.setColumns(10);
		textFieldLand.setBounds(153, 233, 325, 17);
		contentPane.add(textFieldLand);
		
		JComboBox comboBoxGeschaeftlich = new JComboBox();
		comboBoxGeschaeftlich.setBounds(153, 258, 325, 17);
		contentPane.add(comboBoxGeschaeftlich);
		
		JButton btnPersonendatenLaden = new JButton("Personendaten laden");
		btnPersonendatenLaden.setBounds(153, 380, 185, 23);
		contentPane.add(btnPersonendatenLaden);
		
		JButton buttonDatenUpdate = new JButton("Personendaten Update");
		buttonDatenUpdate.setBounds(153, 406, 185, 23);
		contentPane.add(buttonDatenUpdate);
		
		JButton buttonNeuAblegen = new JButton("Neu ablegen!");
		buttonNeuAblegen.setBounds(348, 380, 130, 23);
		contentPane.add(buttonNeuAblegen);
		
		JLabel labelSprache = new JLabel("Sprache");
		labelSprache.setBounds(10, 352, 133, 14);
		contentPane.add(labelSprache);
		
		JComboBox comboBoxSprache = new JComboBox();
		comboBoxSprache.setBounds(153, 352, 325, 17);
		contentPane.add(comboBoxSprache);
		
		JButton buttonZuruecksetzen = new JButton("Zuruecksetzen!");
		buttonZuruecksetzen.setBounds(348, 406, 130, 23);
		contentPane.add(buttonZuruecksetzen);
		
		JButton buttonAuswaehlen = new JButton("Ausw�hlen -->");
		buttonAuswaehlen.setBounds(348, 440, 130, 23);
		contentPane.add(buttonAuswaehlen);
		
		textFieldEmail = new JTextField();
		textFieldEmail.setColumns(10);
		textFieldEmail.setBounds(153, 108, 325, 17);
		contentPane.add(textFieldEmail);
		
		JLabel labelEmail = new JLabel("Email");
		labelEmail.setBounds(10, 111, 133, 14);
		contentPane.add(labelEmail);
		
		JLabel labelTelefon = new JLabel("Telefon");
		labelTelefon.setBounds(10, 289, 133, 14);
		contentPane.add(labelTelefon);
		
		textFieldTelefon = new JTextField();
		textFieldTelefon.setColumns(10);
		textFieldTelefon.setBounds(153, 286, 325, 17);
		contentPane.add(textFieldTelefon);
		
		JLabel labelKreditkarte = new JLabel("Kreditkarte");
		labelKreditkarte.setBounds(10, 317, 133, 14);
		contentPane.add(labelKreditkarte);
		
		textFieldKreditkartennummer = new JTextField();
		textFieldKreditkartennummer.setColumns(10);
		textFieldKreditkartennummer.setBounds(256, 314, 176, 17);
		contentPane.add(textFieldKreditkartennummer);
		
		JComboBox comboBoxKreditkartenanbieter = new JComboBox();
		comboBoxKreditkartenanbieter.setBounds(153, 314, 93, 17);
		contentPane.add(comboBoxKreditkartenanbieter);
		
		textFieldKreditkarteSicherheit = new JTextField();
		textFieldKreditkarteSicherheit.setColumns(10);
		textFieldKreditkarteSicherheit.setBounds(442, 314, 36, 17);
		contentPane.add(textFieldKreditkarteSicherheit);
		
		
		// ***************** Eigene Anpassungen *****************		
		
		buttonNeuAblegen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		buttonZuruecksetzen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		buttonDatenUpdate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		buttonAuswaehlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GUI_Buchung.setLableBuchungsPerson(comboBoxVornameNachname.getSelectedItem().toString());
				GUI_Buchung.setPersonID(Integer.parseInt(persId));
				framePerson.setVisible(false);
			}
		});
		
		// Add ActionListener to the comboBox which chooses the name. If name is chosen, then load adress.
		btnPersonendatenLaden.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object selObj = comboBoxVornameNachname.getSelectedItem();
				if(selObj != null) {
					// Split the field into vorname, nachname, person_id
					String pers = selObj.toString();
					String[] persArr = pers.split(nameSeparator);
					persId = persArr[2];
					//for(String a : persArr) { System.out.println(a); }
										
					try {
						// create new SQLUtils and select all rows from the table "adresse" that match the persons id.
						SQLUtils dbTmp = createNewSQLUtils();
						
						// Fill in adress
						final DataFrame adrData = dbTmp.executeQuery("select * from adresse where adresse.person_id = " + persId);
						adrData.printTable();
						// If there are no rows, then signalize with JOptionPane.
						if(adrData.getNrow() == 0){
							SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, "Keine Adresse erfasst f�r diese Person!", "Keine Adresse", JOptionPane.INFORMATION_MESSAGE) );
							// If there are more than 1 row, give error and don't do anything.
						} else if(adrData.getNrow() > 1){
							SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, "Da es mehrere Adressen gibt, kann sie nicht geladen werden!", "Mehrere Adressen", JOptionPane.INFORMATION_MESSAGE) );
							// Else fill in the forms.
						} else {
							SwingUtilities.invokeLater( () -> {
								textFieldStrasse.setText( adrData.getVectorFromCol("strasse")[0].toString() );
								textFieldStrasseZusatz.setText( adrData.getVectorFromCol("strasse2")[0].toString() );
								textFieldPostleitzahl.setText( adrData.getVectorFromCol("postleitzahl")[0].toString() );
								textFieldOrt.setText( adrData.getVectorFromCol("ort")[0].toString() );
								textFieldLand.setText( adrData.getVectorFromCol("land")[0].toString() );
								comboBoxGeschaeftlich.setSelectedIndex( (boolean) adrData.getVectorFromCol("geschaeftlich")[0] ? 1 : 0 );
							});
						}
						
						// Fill in email etc.
						DataFrame emailData = dbTmp.executeQuery("select adresse from email where email.person_id = " + persId);
						if(emailData.getNrow()>0){
							SwingUtilities.invokeLater( () -> textFieldEmail.setText( pasteArray("; ", emailData.getVectorFromCol(0) ) )  );
						}
						dbTmp.closeConnection();
						
						
					} catch (SQLException e1) { showExceptionVisually(e1); }
					
				}
			}
		});		

		
		// Add values into the comboBoxes
		new Thread( () -> {
			HashMap<String, Object[]> stringArrayContainer = new HashMap<>();
			try{
				// Load data base connection.
				SQLUtils db = createNewSQLUtils();
				// Get Data
				stringArrayContainer.put("sprache", db.executeQuery("select beschreibung from sprache").getVectorFromCol(0) );
				stringArrayContainer.put("anrede", db.executeQuery("select wort from anrede").getVectorFromCol(0) );
				stringArrayContainer.put("vornameNachname", db.executeQuery("select concat(nachname, '" +nameSeparator+ "', vorname, '" +nameSeparator+ "', id ) from person").getVectorFromCol(0) );
				stringArrayContainer.put("KreditkartenAnbieter", db.executeQuery("select name from kartenanbieter").getVectorFromCol(0) );
				if(db != null) db.closeConnection();
			} catch(Exception e) { showExceptionVisually(e); }

			SwingUtilities.invokeLater(() -> {
				comboBoxGeschaeftlich.setModel(new DefaultComboBoxModel( new Integer[]{0,1} ));
				comboBoxSprache.setModel(new DefaultComboBoxModel( stringArrayContainer.get("sprache") ));
				comboBoxAnrede.setModel(new DefaultComboBoxModel( stringArrayContainer.get("anrede") ));
				comboBoxKreditkartenanbieter.setModel(new DefaultComboBoxModel( stringArrayContainer.get("KreditkartenAnbieter") ));
				AutoCompleteSupport.install(comboBoxVornameNachname, GlazedLists.eventListOf(stringArrayContainer.get("vornameNachname")));
			});
		}).start();
		
	} // End of initialize
	
	
	private String pasteArray(String delimiter, Object[] arr){
		StringBuilder builder = new StringBuilder();
		for(int i=0; i<arr.length; i++) {
		    builder.append( arr[i] );
		    if(i < arr.length-1){
		    	builder.append( delimiter );
		    }
		}
		return builder.toString();
	}
	private void showExceptionVisually(Exception e, String message){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.INFORMATION_MESSAGE) );
		e.printStackTrace();
	}
	private void showExceptionVisually(Exception e){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE) );		
	}
	private SQLUtils createNewSQLUtils(){
		try { return new SQLUtils("com.mysql.jdbc.Driver",
					"jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017");
		} catch (Exception e) { showExceptionVisually(e); }
		return null;
	}
}
