package ch.ffhs.dbs.fs2016;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.mysql.jdbc.PreparedStatement;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class GUI_Buchung extends JFrame {

	private JPanel contentPane;
	private JButton btnBuchungsPerson;
	private JButton btnBegleitperson;
	private static JLabel lblBuchungsPerson = new JLabel();
	private static JLabel lblBegleitPerson = new JLabel();
	private JLabel lblAbreise;
	private JButton btnBuchen;
	private static int personID = 0;
	


	public GUI_Buchung(int zimmerID, @SuppressWarnings("unused") int anzahlPersonen, String anreise, String abreise) {
		
		//INFOS zu der Person wird sp�ter �bergeben
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Zimmernummer : " + zimmerID);
		lblNewLabel.setBounds(10, 69, 153, 14);
		contentPane.add(lblNewLabel);
		
		JLabel label = new JLabel("Anreise : " + anreise);
		label.setBounds(10, 99, 137, 14);
		contentPane.add(label);
		
		lblAbreise = new JLabel("Abreise : " + abreise);
		lblAbreise.setBounds(10, 124, 118, 14);
		contentPane.add(lblAbreise);
		
		btnBuchungsPerson = new JButton("Person hinzuf\u00FCgen");
		btnBuchungsPerson.setBounds(10, 11, 180, 23);
		contentPane.add(btnBuchungsPerson);
		
		btnBegleitperson = new JButton("Begleitperson hinzuf\u00FCgen");
		btnBegleitperson.setBounds(10, 35, 180, 23);
		contentPane.add(btnBegleitperson);
		
		lblBuchungsPerson.setBounds(205, 11, 208, 14);
		contentPane.add(lblBuchungsPerson);
		
		lblBegleitPerson.setBounds(205, 35, 66, 14);
		contentPane.add(lblBegleitPerson);
		
		btnBuchen = new JButton("Buchen");
		btnBuchen.setBounds(10, 149, 89, 23);
		contentPane.add(btnBuchen);
		
		btnBuchungsPerson.addActionListener(new ActionListener() {
			@SuppressWarnings("unused")
			public void actionPerformed(ActionEvent arg0) {
				try {
					new GUI_PersonAdresse();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		btnBuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				try{
					/*Datum Umwandeln*/	
					Date anreiseEingabe = new SimpleDateFormat("dd.MM.yyyy").parse(anreise);
					Date abreiseEingabe = new SimpleDateFormat("dd.MM.yyyy").parse(abreise);
					DateFormat dateAbfrage = new SimpleDateFormat("yyyy-MM-dd");	
					String anreiseAbfrage = dateAbfrage.format(anreiseEingabe).toString();
					String abreiseAbfrage = dateAbfrage.format(abreiseEingabe).toString();
					
					 String connectString = "jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true";
					 String user = "dbs";		
					 String password = "db$s-ffh!s.FS2017";					
					
						try {
							/*Buchung Eintragen*/
							Connection con = DriverManager.getConnection(connectString, user, password);
							String stringBuchung = "insert into buchung (kunde_id, anzahl_personen, datum_anreise, datum_abreise) values(1, 2, '" + anreiseAbfrage + "', '" + abreiseAbfrage + "');";						
							
							PreparedStatement worker;
							worker = (PreparedStatement) con.prepareStatement(stringBuchung, Statement.RETURN_GENERATED_KEYS);
							worker.executeUpdate();
							
							ResultSet rsID = worker.getGeneratedKeys();
							/*Buchung ID auslesen*/
							int buchung_id = 0;
			                if(rsID.next())
			                {
			                	buchung_id = rsID.getInt(1);
			                }
			                rsID.close();
			                System.out.println("Buchung ID : " + buchung_id);
			                
			                /*zimmer_verkn Eintragen*/
			            	String stringZimmerVerkn = "insert into zimmer_verkn values(" + zimmerID + ", " + buchung_id + ");";						
			            	worker.executeUpdate(stringZimmerVerkn); 
			            	System.out.println(stringZimmerVerkn);
			            	
			                /*begleitperosn_verkn Eintragen*/
			            	String stringBegleitperosn_verknn = "insert into begleitperson_verkn values(" + buchung_id + ", " + personID + ");";						
			            	worker.executeUpdate(stringBegleitperosn_verknn); 
			            	System.out.println(stringBegleitperosn_verknn);
			                
			            	worker.close();
			    			con.close();
						} 
						catch (SQLException e) {
							e.printStackTrace();
						}
									} 
				catch(Exception e) { 
					showExceptionVisually(e); 
					}
		}
		});

	}
	
    public static void setLableBuchungsPerson(String string) {
    	lblBuchungsPerson.setText(string);
    }
    
    public static void setLableBegleitPerson(String string) {
    	lblBegleitPerson.setText(string);
    }
    
	public static void setPersonID(int persId) {
		personID = persId;
		System.out.println(personID);
	}
	
	private void showExceptionVisually(Exception e){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE) );		
	}

}
