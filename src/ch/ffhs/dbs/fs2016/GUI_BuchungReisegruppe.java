package ch.ffhs.dbs.fs2016;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;



import ch.danielhoop.sql.DataFrame;
import ch.danielhoop.sql.SQLUtils;

public class GUI_BuchungReisegruppe extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldAnkunft;
	private JTextField textFieldAbreise;
	private JTextField textFieldAnzPers;
	private JButton btnSuchen;
	private JLabel lblFehlerDate;
	private JTable table;
	private DefaultTableModel tableModel;
	private JLabel lblFehlerAnzahl;
	private JButton btnBuchen;
	private ArrayList<Integer> buchungZimmerID = new ArrayList<Integer>();


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LookAndFeel.set();
					GUI_BuchungReisegruppe frame = new GUI_BuchungReisegruppe();
					frame.setVisible(true);
				} catch (Exception e) { showExceptionVisually(e); }
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_BuchungReisegruppe() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 788, 413);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblAnkunft = new JLabel("Ankunft");
		lblAnkunft.setBounds(10, 10, 50, 14);
		contentPane.add(lblAnkunft);

		textFieldAnkunft = new JTextField();
		textFieldAnkunft.setBounds(120, 10, 90, 20);
		contentPane.add(textFieldAnkunft);
		textFieldAnkunft.setColumns(10);
		textFieldAnkunft.setText("14.01.2017");	//Zum Testen

		JLabel lblAbreise = new JLabel("Abreise");
		lblAbreise.setBounds(10, 40, 50, 14);
		contentPane.add(lblAbreise);

		textFieldAbreise = new JTextField();
		textFieldAbreise.setBounds(120, 40, 90, 20);
		contentPane.add(textFieldAbreise);
		textFieldAbreise.setColumns(10);
		textFieldAbreise.setText("16.01.2017");	//Zum Testen

		JLabel lblAnzPers = new JLabel("Anzahl Personen");
		lblAnzPers.setBounds(10, 70, 100, 14);
		contentPane.add(lblAnzPers);

		lblFehlerAnzahl = new JLabel("Nicht gen\u00FCgend freie Betten f\u00FCr die Anzahl Personen");
		lblFehlerAnzahl.setForeground(Color.RED);
		lblFehlerAnzahl.setBounds(223, 73, 320, 14);
		contentPane.add(lblFehlerAnzahl);
		lblFehlerAnzahl.setVisible(false);

		textFieldAnzPers = new JTextField();
		textFieldAnzPers.setBounds(120, 70, 30, 20);
		contentPane.add(textFieldAnzPers);
		textFieldAnzPers.setColumns(10);

		btnSuchen = new JButton("Suchen");
		btnSuchen.setBounds(120, 100, 90, 20);
		contentPane.add(btnSuchen);

		lblFehlerDate = new JLabel("Eingabedaten \u00FCberpr\u00FCfen");
		lblFehlerDate.setForeground(Color.RED);
		lblFehlerDate.setBounds(223, 103, 294, 14);
		contentPane.add(lblFehlerDate);
		lblFehlerDate.setVisible(false);

		table = new JTable();
		tableModel = new DefaultTableModel(new Object[0][7], new String[]{"Zimmer ID","Anzahl Betten","Minibar","Zimmer hat Alpenblick","Nichtraucher Zimmer","Stockwerk","Gebäudeteil"} );
		table.setModel(tableModel);
		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(5, 143, 765, 184);
		contentPane.add(scrollPane);

		btnBuchen = new JButton("Buchen");
		btnBuchen.setBounds(673, 338, 89, 23);
		contentPane.add(btnBuchen);


		btnSuchen.addActionListener(new ActionListener() {
			@SuppressWarnings("null")
			public void actionPerformed(ActionEvent e) {
				SQLUtils db = null;
				try{
					db = new SQLUtils("com.mysql.jdbc.Driver",
							"jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017" );
					//"jdbc:mysql://localhost:3306/lucius?verifyServerCertificate=false&useSSL=false","root","ffhs" );
				} 
				catch(Exception e1) { 
					showExceptionVisually(e1); 
				}
				try {
					java.util.Date dateAnreise = null;
					java.util.Date dateAbreise = null;
					String anreiseEingabe = textFieldAnkunft.getText();
					String abreiseEingabe = textFieldAbreise.getText();
					int anzahlPersonenEingabe = Integer.parseInt(textFieldAnzPers.getText());			

					SimpleDateFormat dateEingaben = new SimpleDateFormat("dd.MM.yyyy");

					try {
						dateAnreise = dateEingaben.parse(anreiseEingabe);
						dateAbreise = dateEingaben.parse(abreiseEingabe);
					} catch (ParseException e1) {
						System.out.println("Datum falsches Format");
					}

					if((dateAnreise.before(dateAbreise) || dateAbreise.equals(dateAnreise)) && anzahlPersonenEingabe > 0){
						lblFehlerDate.setVisible(false);
						lblFehlerAnzahl.setVisible(false);

						// PreparedStatement
						java.sql.PreparedStatement prst = db.getConnection().prepareStatement(""
								+ "SELECT z.id AS 'Zimmer ID',abz.anzahl AS 'Anzahl Betten',z.hat_minibar AS Minibar,z.hat_alpenblick AS 'Zimmer hat Alpenblick', z.ist_nichtraucher AS 'Nichtraucher Zimmer', z.stockwerk AS Stockwerk, g.name AS Gebäudeteil "
								+ "FROM zimmer z, anzahlBettZimmer abz, gebaeudeteil g "
								+ "WHERE z.id NOT IN (SELECT z.id "
								+ "FROM buchung b, zimmer z, zimmer_verkn zv, anzahlBettZimmer abz "
								+ "WHERE zv.zimmer_id = z.id AND zv.buchung_id = b.id AND z.anzahlBettZimmer_id = abz.id "
								+ "AND (datum_anreise < '" + abreiseEingabe + "' "
								+ "AND datum_abreise > '" + anreiseEingabe + "')) AND z.anzahlBettZimmer_id = abz.id AND g.id = z.gebaeudeteil_id "
								+ "HAVING abz.anzahl = ?");

						// Query for 1, 2, 3 person rooms.
						prst.setInt(1, 1);
						DataFrame df1 = SQLUtils.resultSetToDataFrame( prst.executeQuery() );
						prst.setInt(1, 2);
						DataFrame df2 = SQLUtils.resultSetToDataFrame( prst.executeQuery() );
						prst.setInt(1, 3);
						DataFrame df3 = SQLUtils.resultSetToDataFrame( prst.executeQuery() );
						prst.close();

						// Prepare Indexes which will get the right rows.
						ArrayList<Integer> ind1 = new ArrayList<>();
						ArrayList<Integer> ind2 = new ArrayList<>();
						ArrayList<Integer> ind3 = new ArrayList<>();
						System.out.println("ind1 : " + ind1.size());

						// Get the indexes for the right rows.
						int anzahlBetten = 0;
						int diff = anzahlPersonenEingabe - anzahlBetten;
						while( diff > 0 ){
							if( diff>2   &&   ind3.size() < df3.getNrow() ) {
								ind3.add( ind3.size() );
								diff -= 3;
							} else if ( diff>1   &&   ind2.size() < df2.getNrow() ){
								ind2.add( ind2.size() );
								diff -= 2;
							} else if ( ind1.size() < df1.getNrow() ){
								ind1.add( ind1.size() );
								diff -= 1;
							} else {
								lblFehlerAnzahl.setVisible(true);
								System.out.println("Keine Freien Zimmer!");
								break;
							}
						}

						// Create new Data Frame with right rooms.
						DataFrame df9 = DataFrame.combineDataFrames(new DataFrame[]{
								df1.getDataFrameFromRows( ind1.stream().mapToInt(i -> i).toArray() ),
								df2.getDataFrameFromRows( ind2.stream().mapToInt(i -> i).toArray() ),
								df3.getDataFrameFromRows( ind3.stream().mapToInt(i -> i).toArray() )
						});

						// Add data frame to the table model.
						df9.printTable();
						String[] beschriftung = {"Zimmer ID","Anzahl Betten","Minibar","Zimmer hat Alpenblick","Nichtraucher Zimmer","Stockwerk","Gebäudeteil"};
						tableModel.setDataVector(df9.getTable(), beschriftung);

						// Save all Found Rooms in a Arraylist to save in zimmer_verk DB
						buchungZimmerID.clear();
						buchungZimmerID.addAll( java.util.Arrays.asList( Arrays.copyOf( df9.getVectorFromCol("id"), df9.getVectorFromCol("id").length, Integer[].class )));

					} else {
						lblFehlerDate.setVisible(true);
						tableModel.setRowCount(0);
						System.out.println("Abreise Grösser wie Anreise!");
					}

				} catch (Exception e1) { showExceptionVisually(e1); }
			}
		});
		btnBuchen.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				Date anreiseEingabe = null;
				Date abreiseEingabe = null;
				try {
					anreiseEingabe = new SimpleDateFormat("dd.MM.yyyy").parse(textFieldAnkunft.getText());
					abreiseEingabe = new SimpleDateFormat("dd.MM.yyyy").parse(textFieldAbreise.getText());
				} catch (ParseException e2) {
					// TODO Auto-generated catch block
					e2.printStackTrace();
				}
				DateFormat dateAbfrage = new SimpleDateFormat("yyyy-MM-dd");	
				String anreiseAbfrage = dateAbfrage.format(anreiseEingabe).toString();
				String abreiseAbfrage = dateAbfrage.format(abreiseEingabe).toString();
				int anzahlPersonenEingabe = Integer.parseInt(textFieldAnzPers.getText());


				System.out.println("Eingabe : " + anreiseAbfrage);		
				System.out.println("Eingabe : " + abreiseAbfrage);		

				String connectString = "jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true";
				String user = "dbs";		
				String password = "db$s-ffh!s.FS2017";
				
				try {
					/*Buchung Eintragen*/
					Connection con = DriverManager.getConnection(connectString, user, password);
					String stringBuchung = "insert into buchung (kunde_id, anzahl_personen, datum_anreise, datum_abreise) values (1, " + anzahlPersonenEingabe + ", '" + anreiseAbfrage + "', '" + abreiseAbfrage + "');";						
					System.out.println(stringBuchung);
					PreparedStatement worker;
					worker = con.prepareStatement(stringBuchung, Statement.RETURN_GENERATED_KEYS);
					worker.executeUpdate();

					ResultSet rsID = worker.getGeneratedKeys();	//ID der Buchung auslesen
					/*Buchung ID auslesen*/
					int buchung_id = 0;
					if(rsID.next()) {
						buchung_id = rsID.getInt(1);
						System.out.println(rsID.getInt(1));
					}
					rsID.close();

					/*zimmer_verkn Eintragen*/
					for(int id : buchungZimmerID){
						String stringZimmerVerkn = "insert into zimmer_verkn values (" + id + ", " + buchung_id + ");";						
						worker.executeUpdate(stringZimmerVerkn); 
						System.out.println(stringZimmerVerkn);
						System.out.println(id);
					}
					worker.close();
					con.close();
				} catch (SQLException e1) { showExceptionVisually(e1); }
			}
		});
	}

	private static void showExceptionVisually(Exception e, String message){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.INFORMATION_MESSAGE) );
		e.printStackTrace();
	}
	private static void showExceptionVisually(Exception e){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE) );		
	}
	private SQLUtils createNewSQLUtils(){
		try { return new SQLUtils("com.mysql.jdbc.Driver",
				"jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017");
		} catch (Exception e) { showExceptionVisually(e); }
		return null;
	}

}
