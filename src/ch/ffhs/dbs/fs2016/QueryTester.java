package ch.ffhs.dbs.fs2016;

import java.sql.SQLException;

import ch.danielhoop.sql.DataFrame;
import ch.danielhoop.sql.SQLUtils;


public class QueryTester {

	public static void main(String[] args){
		SQLUtils worker = null;
		try{
			worker = new SQLUtils(
					"com.mysql.jdbc.Driver",
					//"jdbc:mysql://localhost:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true", "root", "????"
					"jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017"
					//"jdbc:mysql://192.168.0.192:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017"
					);

			
			
//			worker.executeUpdate("insert into person"
//					+         "(vorname, nachname, geburtsdatum, ist_hotelmitarbeiter, sprache_id)"
//					+ "value  ('java1', 'java',   '1989-02-11', 0,                     1)");
//			worker.executeUpdate( "delete from person where vorname = 'java1'" );
			
			if(false){
				worker.executeUpdate(" insert into kreditkarte (person_id, name_besitzer, nummer, gueltig_bis, sicherheitsnummer, kartenanbieter_id) values (2,'Heinz Meier', '4563 4563 7894 1256', '2020-03-01', 132, 2); " );
				worker.executeQuery(" select * from kreditkarte; ").printTable();
				worker.executeUpdate(" delete from kreditkarte where id = 8; ");
				worker.executeQuery(" select * from kreditkarte; ").printTable();
			}
			
			
			if(false){
				DataFrame data = worker.executeQuery("select * from person;");
				System.out.println(data.getNrow());
				//System.out.println( data.getTable(0, new String[]{"vorname","nachname"} )[0][1] );
				//System.out.println( data.getVector(0, new String[]{"vorname","nachname"} )[0] );
				data.printTable();
				data.printCol("vorname");
				data.printRow(0);
				
				worker.executeQuery("select concat(vorname, ' ', nachname) from person").printTable();
				worker.executeQuery("select * from adresse").printTable();
			}
			
			worker.executeQuery(" select * from buchung; ").printTable();
			System.out.println(" ---------------------------- ");
			worker.executeQuery(" select * from zimmer_verkn order by buchung_id asc; ").printTable();
			// worker.executeQuery("select * from begleitperson_verkn;").printTable();
			
			// Info: Erst muss die Zimmerverkn�pfung geloescht werden, dann die Buchung!
			// worker.executeUpdate(" delete from zimmer_verkn where buchung_id = 14; delete from begleitperson_verkn where buchung_id = 14; delete from buchung where id = 14;  ");
			
			
			
			
			
			
			
						
			


		} catch(ClassNotFoundException e) {
			System.out.println("ClassNotFoundException");
			e.printStackTrace();
		} catch(InstantiationException e) {
			System.out.println("InstantiationException");
			e.printStackTrace();
		} catch(IllegalAccessException e) {
			System.out.println("IllegalAccessException");
			e.printStackTrace();
		} catch(SQLException e) {
			System.out.println("SQLException");
			e.printStackTrace();
		}

		
		if(worker != null) {
			worker.closeConnection();
		}


		//		DataFrame df = new DataFrame(new Object[][]{ {1,2}, {3,4} }, new String[]{"a","b"}, new Class<?>[]{Integer.class, Integer.class});
		//		df.printTable();
		//		DataFrame.printVector( df.getVectorFromCol("a") );


	}
}
