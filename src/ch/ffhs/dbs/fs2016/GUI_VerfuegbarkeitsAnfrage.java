package ch.ffhs.dbs.fs2016;

import java.awt.EventQueue;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import ch.danielhoop.sql.DataFrame;
import ch.danielhoop.sql.SQLUtils;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.JButton;
import java.awt.Color;

@SuppressWarnings("serial")
public class GUI_VerfuegbarkeitsAnfrage extends JFrame {

	private JScrollPane scrollPane;
	private JPanel contentPane;
	private JTextField textField_Anreise;
	private JTextField textField_Abreise;
	private JTextField textField_AnzPersonen;
	
    private JTable table;
    private DefaultTableModel tableModel;
    private JLabel lblDdmmyyyy;
    private JLabel label;
    private JLabel lblFehlerDate;
    
	static GUI_VerfuegbarkeitsAnfrage frame = new GUI_VerfuegbarkeitsAnfrage();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LookAndFeel.set();
					//GUI_VerfuegbarkeitsAnfrage frame = new GUI_VerfuegbarkeitsAnfrage();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_VerfuegbarkeitsAnfrage() {
		
		super();
		setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		//JFrame frame = new JFrame("Magician"); 
		setTitle("Verf\u00FCgbarkeitsanfrage");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 793, 376);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Anreise");
		lblNewLabel.setBounds(10, 10, 100, 14);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Abreise");
		lblNewLabel_1.setBounds(10, 40, 100, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Anzahl Personen");
		lblNewLabel_2.setBounds(10, 70, 100, 14);
		contentPane.add(lblNewLabel_2);
		
		textField_Anreise = new JTextField();
		textField_Anreise.setBounds(120, 7, 86, 20);
		contentPane.add(textField_Anreise);
		textField_Anreise.setColumns(10);
		textField_Anreise.setText("14.01.2017");	//Zum Testen
		
		textField_Abreise = new JTextField();
		textField_Abreise.setBounds(120, 37, 86, 20);
		contentPane.add(textField_Abreise);
		textField_Abreise.setColumns(10);
		textField_Abreise.setText("16.01.2017");	//Zum Testen
		
		textField_AnzPersonen = new JTextField();
		textField_AnzPersonen.setBounds(120, 67, 25, 20);
		contentPane.add(textField_AnzPersonen);
		textField_AnzPersonen.setColumns(10);
		textField_AnzPersonen.setText("2");	//Zum Testen
		
		
		JButton btnSuchen = new JButton("Suchen");
		btnSuchen.setBounds(10, 109, 89, 23);
		contentPane.add(btnSuchen);

		
        table = new JTable();
        tableModel = new DefaultTableModel(
        		new Object[0][7],
        		new String[]{"Zimmer ID","Anzahl Betten","Minibar","Zimmer hat Alpenblick","Nichtraucher Zimmer","Stockwerk","Gebäudeteil"});
        table.setModel(tableModel);
        //table.setBounds(5, 143, 765, 184);
        //contentPane.add(table);
        
        scrollPane = new JScrollPane(table);
        scrollPane.setBounds(5, 143, 765, 184);
        contentPane.add(scrollPane);
        
        lblDdmmyyyy = new JLabel("dd.MM.yyyy");
        lblDdmmyyyy.setBounds(216, 10, 70, 14);
        contentPane.add(lblDdmmyyyy);
        
        label = new JLabel("dd.MM.yyyy");
        label.setBounds(216, 40, 70, 14);
        contentPane.add(label);
        
        lblFehlerDate = new JLabel("Eingabedaten \u00FCberpr\u00FCfen");
        lblFehlerDate.setForeground(Color.RED);
        lblFehlerDate.setBounds(120, 113, 294, 14);
        contentPane.add(lblFehlerDate);
        lblFehlerDate.setVisible(false);
		
		btnSuchen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				SQLUtils db = null;
				try{
					db = new SQLUtils("com.mysql.jdbc.Driver",
						"jdbc:mysql://danielhoop.goip.de:3306/lucius?autoReconnect=true&verifyServerCertificate=false&useSSL=true","dbs","db$s-ffh!s.FS2017" );
						//"jdbc:mysql://localhost:3306/lucius?verifyServerCertificate=false&useSSL=false","root","ffhs" );
				} 
				catch(Exception e) { 
					showExceptionVisually(e); 
					}
				try {
			    	java.util.Date dateAnreise = null;
			    	java.util.Date dateAbreise = null;
					String anreiseEingabe = textField_Anreise.getText();
					String abreiseEingabe = textField_Abreise.getText();
					String anzahlPersonenEingabe = textField_AnzPersonen.getText();			

					SimpleDateFormat dateEingaben = new SimpleDateFormat("dd.MM.yyyy");

					try {
						dateAnreise = dateEingaben.parse(anreiseEingabe);
						dateAbreise = dateEingaben.parse(abreiseEingabe);
					} catch (ParseException e1) {
						System.out.println("Datum falsches Format");
					}
					
					DateFormat dateAbfrage = new SimpleDateFormat("yyyy-MM-dd");	
					String anreiseAbfrage = dateAbfrage.format(dateAnreise).toString();
					String abreiseAbfrage = dateAbfrage.format(dateAbreise).toString();
					int anzahlPersonenInt = Integer.parseInt(anzahlPersonenEingabe);
					
					System.out.println("an: " + anreiseAbfrage);
					System.out.println("ab: " + abreiseAbfrage);
					
					if((dateAnreise.before(dateAbreise) || dateAbreise.equals(dateAnreise))&& anzahlPersonenInt > 0){
						lblFehlerDate.setVisible(false);	
			
						DataFrame ZimmerData = db.executeQuery(
								"SELECT "
										+ "    z.id AS 'Zimmer ID', "
										+ "    abz.anzahl AS 'Anzahl Betten', "
										+ "    z.hat_minibar AS Minibar,"
										+ "    z.hat_alpenblick AS 'Zimmer hat Alpenblick', "
										+ "    z.ist_nichtraucher AS 'Nichtraucher Zimmer', "
										+ "    z.stockwerk AS Stockwerk, "
										+ "    g.name AS Gebäudeteil "
										+ "FROM "
										+ "    zimmer z,"
										+ "    anzahlBettZimmer abz,"
										+ "    gebaeudeteil g "
										+ "WHERE "
										+ "    z.id NOT IN ("
										+ "		SELECT "
										+ "		z.id "
										+ "		FROM "
										+ "			buchung b, "
										+ "			zimmer z, "
										+ "			zimmer_verkn zv "
										+ "		WHERE "
										+ "			b.id = zv.buchung_id AND "
										+ "			zv.zimmer_id = z.id AND "
										+ "			(datum_anreise < '" + abreiseAbfrage + "' AND datum_abreise > '" + anreiseAbfrage + "') "
										+ "        ) "
										+ "        AND z.anzahlBettZimmer_id = abz.id "
										+ "        AND g.id = z.gebaeudeteil_id "
										+ "HAVING abz.anzahl = " + anzahlPersonenInt + ";"
								);
						
						tableModel.setDataVector(
								ZimmerData.getTable(),
								new String[]{"Zimmer ID","Anzahl Betten","Minibar","Zimmer hat Alpenblick","Nichtraucher Zimmer","Stockwerk","Gebäudeteil"}); 
					}

					else{
						lblFehlerDate.setVisible(true);
						tableModel.setRowCount(0);
						System.out.println("Abreise Grösser wie Anreise!");
					}

				} catch (Exception e) {
					e.printStackTrace();				
				}
			}
		});
		table.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        String zimmerID = table.getValueAt(row,0).toString();
		        if (me.getClickCount() == 2) {
		        	System.out.println("DoubleClick :" + zimmerID);
		        }
		    }
		});
		table.addMouseListener(new MouseAdapter() {
		    public void mousePressed(MouseEvent me) {
		        JTable table =(JTable) me.getSource();
		        Point p = me.getPoint();
		        int row = table.rowAtPoint(p);
		        String zimmer = table.getValueAt(row,0).toString();
		        if (me.getClickCount() == 2) {
		        	System.out.println("DoubleClick :" + zimmer);
		        	GUI_Buchung guiBuchung = new GUI_Buchung(Integer.parseInt(zimmer), Integer.parseInt(textField_AnzPersonen.getText()), textField_Anreise.getText(),  textField_Abreise.getText());
		        	guiBuchung.setVisible(true);
		        	
		        	frame.setVisible(false);
		        }
		    }
		});
	}

	private void showExceptionVisually(Exception e){
		SwingUtilities.invokeLater( () -> JOptionPane.showMessageDialog(null, e.getMessage(), "Error", JOptionPane.INFORMATION_MESSAGE) );		
	}
}
