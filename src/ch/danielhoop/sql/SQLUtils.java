
package ch.danielhoop.sql;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;



public class SQLUtils {
	private Connection conn;
	private Statement st;
	private ResultSet rs;
	private String dbLoginAdress, dbUser, dbPassword;
	
	
	/*
	 * How to use this class.
	 */
	public static void main(String[] args){
		
				
	}



	public SQLUtils(String dbDriver, String dbLoginAdress, String dbUser, String dbPassword, boolean connect)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {

		System.out.println("Loading database driver...");
		Class.forName(dbDriver).newInstance();
		System.out.println("Loaded.");

		this.dbLoginAdress = dbLoginAdress;
		this.dbUser = dbUser;
		this.dbPassword = dbPassword;

		if(connect){
			openConnection();
		}
	}

	public SQLUtils(String dbDriver, String dbLoginAdress, String dbUser, String dbPassword)
			throws SQLException, ClassNotFoundException, InstantiationException, IllegalAccessException {
		this(dbDriver, dbLoginAdress, dbUser, dbPassword, true);
	}

	public void openConnection() throws SQLException {
		if(conn == null || conn.isClosed()){
			System.out.println("Connecting to database...");
			conn = DriverManager.getConnection(dbLoginAdress, dbUser, dbPassword);
			conn.setAutoCommit(false);
			st = conn.createStatement();
			System.out.println("Connected.");
		} else {
			System.out.println("Connecting not necessary. Already connected!");
		}

	}

	public void closeConnection() {
		try {
			if(st != null) {
				st.close(); st = null;
			}
			if(conn != null){
				conn.close(); conn = null;
			}
			System.out.println("Connection & statement closed.");
		} catch (SQLException e) { e.printStackTrace(); }

	}
	
	public Connection getConnection(){
		return conn;
	}

	public DataFrame executeQuery(String sql) throws SQLException {
		System.out.println("Executing query...");
		rs = st.executeQuery( sql );
		System.out.println("Executed.");
		
		return resultSetToDataFrame(rs, true);
	}
	
	public void executeUpdate(String sql) throws SQLException{
		st.executeUpdate(sql);
	}
	
	public static DataFrame resultSetToDataFrame(ResultSet rs) throws SQLException {
		return resultSetToDataFrame(rs, false);
	}
	
	private static DataFrame resultSetToDataFrame(ResultSet rs, boolean closeSetAndSetNull) throws SQLException {
		
		if(rs == null){
			System.out.println("ResultSet was null an therefore null was returned!");
			return null;
		}
		
		ResultSetMetaData meta = rs.getMetaData() ; 
		String[] colnames = new String[meta.getColumnCount()];
		for(int i=0; i<colnames.length; i++){
			colnames[i] = meta.getColumnName(i+1); // Attention. Starts with 1!
		}

		// Show classes in result set.
		//for(int c=0; c<colnames.length; c++){
		//	System.out.println( meta.getColumnClassName(c+1) + "\n" + meta.getColumnType(c+1) + "\n---------------------");
		//}

		ArrayList<Object[]> data = new ArrayList<>();
		Class<?>[] classes = new Class<?>[colnames.length]; 

		int r = -1;
		while(rs.next()){
			r++;
			data.add( new Object[colnames.length] );

			for(int c=0; c<colnames.length; c++){
				if (meta.getColumnClassName(c+1) == "java.lang.Integer") {
					if(r==0) classes[c] = Integer.class;
					data.get(r)[c] = rs.getInt(c+1);
				}  else if (meta.getColumnClassName(c+1) == "java.lang.Boolean") {
					if(r==0) classes[c] = Boolean.class;
					data.get(r)[c] = rs.getBoolean(c+1);
				}  else if (meta.getColumnClassName(c+1) == "java.lang.Short") {
					if(r==0) classes[c] = Short.class;
					data.get(r)[c] = rs.getShort(c+1);
				}  else if (meta.getColumnClassName(c+1) == "java.lang.Long") {
					if(r==0) classes[c] = Long.class;
					data.get(r)[c] = rs.getLong(c+1);
				}  else if (meta.getColumnClassName(c+1) == "java.lang.Double") {
					if(r==0) classes[c] = Double.class;
					data.get(r)[c] = rs.getDouble(c+1);
				} else if(meta.getColumnClassName(c+1) == "java.lang.String") {
					if(r==0) classes[c] = String.class;
					data.get(r)[c] = rs.getString(c+1);
				} else if (meta.getColumnClassName(c+1) == "java.sql.Date") {
					if(r==0) classes[c] = Date.class;
					data.get(r)[c] = rs.getDate(c+1);
				} else {
					if(r==0) classes[c] = Object.class;
					data.get(r)[c] = rs.getObject(c+1);
				}
			}
		}
		if(closeSetAndSetNull) {
			rs.close();
			rs = null;
		}
		
		return new DataFrame( data.toArray(new Object[data.size()][colnames.length]), colnames, classes ); // data.get(0).length
		
	}

}
